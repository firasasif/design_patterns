package structural.facade;

public class PopcornPopper {
	public void on() {
		System.out.println("Popper is turned on");
	}
	
	public void pop() {
		System.out.println("Popper is popping");
	}
	
	public void off() {
		System.out.println("Popper is tuned off");
	}
}
