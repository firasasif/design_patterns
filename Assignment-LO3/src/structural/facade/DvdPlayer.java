package structural.facade;

public class DvdPlayer {
	
	public DvdPlayer() {
		System.out.println("Welcome to Sony DvdPlayer");
	}
	
	public void on() {
		System.out.println("DVD Player is turned on");
	}
	
	public void play(String movie) {
		System.out.println("Playing " + movie);
	}
	
	public void stop() {
		System.out.println("DVD player stopped!");
	}
	
	public void eject() {
		System.out.println("DVD disk ejected!");
	}
	
	public void off() {
		System.out.println("DVD player is turned off");
	}
}
