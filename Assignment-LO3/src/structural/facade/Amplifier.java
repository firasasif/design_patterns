package structural.facade;

public class Amplifier {
	public void on() {
		System.out.println("Amplifier is turned on");
	}
	
//	public void setDvd(DvdPlayer dvd) {
//		System.out.println(new DvdPlayer() + "set");
//	}
	
	public void setVolume(int volume) {
		System.out.println("Volume is set to " + volume);
	}
	
	public void off() {
		System.out.println("Amplifier is turned off");
	}
}
