package structural.facade;

public class Projector {
	public void on() {
		System.out.println("Projecter is turned on");
	}
	
	public void wideScreenMode() {
		System.out.println("Project is set to Wide Screen Mode");
	}
	
	public void off() {
		System.out.println("Projecter is turned off");
	}
}
