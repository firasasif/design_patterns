package structural.facade;

public class Screen {
	
	public void up() {
		System.out.println("Screen is lifted up");
	}
	
	public void down() {
		System.out.println("Screen is lowered");
	}
}
