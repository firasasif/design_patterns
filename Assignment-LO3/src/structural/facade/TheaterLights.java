package structural.facade;

public class TheaterLights {
	
	public void dim(int intensity) {
		System.out.println("Light dimmed "+ intensity+ " %");
	}
	
	public void on() {
		System.out.println("Lights are turned on");
	}
}
