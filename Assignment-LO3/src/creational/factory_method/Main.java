package creational.factory_method;

import java.util.Scanner;

import behavioral.memento.CareTaker;
import behavioral.memento.Originator;
import creational.factory_method.ComputerFactory;
import structural.facade.Amplifier;
import structural.facade.CdPlayer;
import structural.facade.DvdPlayer;
import structural.facade.HomeTheaterFacade;
import structural.facade.PopcornPopper;
import structural.facade.Projector;
import structural.facade.Screen;
import structural.facade.TheaterLights;
import structural.facade.Tuner;

public class Main {

	public static void main(String[] args) {

		
		System.out.println("\n");
		System.out.println("-----------------------------------------\n");
		System.out.println("Please select an option to start working!\n");
		System.out.println("-----------------------------------------\n");
		
		
		
		while(true) {
			
			System.out.println("1. Run Factory Design Pattern Program \n");
			System.out.println("2. Run Facade Design Pattern Program \n");
			System.out.println("3. Run Memento Design Pattern Program \n");
			System.out.println("4. Quit Program \n");
			System.out.println("----------------------------------------");
			
			Scanner myScanner = new Scanner(System.in);
			int option = myScanner.nextInt();
			
			
			switch(option) {
				case 1:
					System.out.println("-----------------Factory Method Program Started-------------------------------\n");
					
					Computer pc = ComputerFactory.getComputer("pc","2 GB","500 GB","2.4 GHz");
					Computer server = ComputerFactory.getComputer("server","16 GB","1 TB","2.9 GHz");
					System.out.println("\tFactory PC Config::"+pc+"\n");
					System.out.println("\tFactory Server Config::"+server+"\n");
					
					System.out.println("-----------------Factory Method Program Ended---------------------------------\n");
					
					break;
					
				case 2: 
					System.out.println("-----------------Facade Pattern Program Started-------------------------------\n");
					
					Amplifier amp = new Amplifier();
					Tuner tuner = new Tuner();
					DvdPlayer dvd = new DvdPlayer();
					CdPlayer cd = new CdPlayer();
					Projector projector = new Projector();
					Screen screen = new Screen();
					TheaterLights lights = new TheaterLights();
					PopcornPopper popper = new PopcornPopper();
					HomeTheaterFacade homeTheater = new HomeTheaterFacade (amp, tuner, dvd, cd, projector, screen, lights, popper);
					homeTheater.watchMovie("Raiders of the Lost Ark\n");
					
					System.out.println("------------------------------------------------------------------------------\n");
					
					homeTheater.endMovie();
					System.out.println("\n");
					
					System.out.println("-----------------Facade Pattern Program Ended Here----------------------------\n");
					
					break;
					
				case 3:
					System.out.println("-----------------Memento Pattern Program Started-------------------------------\n");
					
					Originator originator = new Originator(); 
					CareTaker careTaker = new CareTaker(); 
					originator.setState("State #1"); 
					originator.setState("State #2"); 	careTaker.add(originator.saveStateToMemento()); 
					originator.setState("State #3"); 	careTaker.add(originator.saveStateToMemento()); 
					originator.setState("State #4"); 
					System.out.println("Current State: " + originator.getState()); 	originator.getStateFromMemento(careTaker.get(0)); 	System.out.println("First saved State: " + originator.getState()); 	originator.getStateFromMemento(careTaker.get(1)); 	System.out.println("Second saved State: " + originator.getState());
					
					System.out.println("\n");
					System.out.println("-----------------Memento Pattern Program Ended Here----------------------------\n");
					
					break;
					
				case 4:
					myScanner.close();
					System.exit(0);
					
					break;
					
				default:
					System.out.println("Invalid option!");
					
			}
			
		}
	}

}
