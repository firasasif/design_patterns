package design_patterns;

import java.util.Enumeration;
import java.util.Scanner;

import design_patterns.DecoratorStream.Authenticate;
import design_patterns.DecoratorStream.Core;
import design_patterns.DecoratorStream.LCD;
import design_patterns.DecoratorStream.Scramble;


public class Main {
	@SuppressWarnings("rawtypes")
	public static void main(String[] args) {
		
		while(true) {
			
			System.out.println("\nPlease select a design pattern!\n\n");
			System.out.println("1. Builder Design Pattern \n");
			System.out.println("2. Decorator Design Pattern \n");
			System.out.println("3. Iterator Design Pattern \n");
			System.out.println("4. Quit Program \n");
			
			Scanner myScanner = new Scanner(System.in);
			int choice = myScanner.nextInt();
			
			switch(choice) {
				case 1:
					System.out.println("\t Running Builder Design Pattern\t\n");
					
					// ---- Builder Design Pattern ------ //     
				    User user1 = new User.UserBuilder("John", "Doe").age(30).phone("1234567").address("Dummy address 1234").build();
				    System.out.println(user1);
				    	 
				    User user2 = new User.UserBuilder("Ali", "Azmat").age(40).phone("5655").build();
				    System.out.println(user2);
				    	 
				    User user3 = new User.UserBuilder("Salman", "Khan").build();
				    System.out.println(user3);
				    
				    System.out.println("\n\t Ended Builder Design Pattern\t\n");
				    break;
				case 2:
					System.out.println("\t Running Decorator Design Pattern\t\n");
					
					// ---- Decorator Design Pattern ------ //     
			        LCD stream = new Authenticate( new Scramble( new Core() ) );
			        String[] str = { new String() };
			        stream.write( str );
			        System.out.println( "main: " + str[0] );
			        stream.read( str );
			        
			        System.out.println("\n\t Ended Decorator Design Pattern\t\n");
			        
			        break;
				case 3:
					System.out.println("\t Running Iterator Design Pattern\t\n");
					
					// ---- Iterator Design Pattern ------ //     
				    
				    IntSet set = new IntSet();
			        for (int i=2; i < 10; i += 2) set.add( i );
			        for (int i=1; i < 9; i++)
			            System.out.print( i + "-" + set.isMember( i ) + "  " );

			        // 3. Clients ask the collection object to create many iterator objects
			        IntSet.Iterator it1 = set.createIterator();
			        IntSet.Iterator it2 = set.createIterator();

			        // 4. Clients use the first(), isDone(), next(), currentItem() protocol
			        System.out.print( "\nIterator:    " );
			        for ( it1.first(), it2.first();  ! it1.isDone();  it1.next(), it2.next() )
			            System.out.print( it1.currentItem() + " " + it2.currentItem() + "  " );

			        // Java uses a different collection traversal "idiom" called Enumeration
			        System.out.print( "\nEnumeration: " );
			        for (Enumeration e = set.getHashtable().keys(); e.hasMoreElements(); )
			            System.out.print( e.nextElement() + "  " );
			        
			        System.out.println("\n\t Ended Iterator Design Pattern\t\n");
				case 4:
					myScanner.close();
					System.exit(0);
					
					break;
					
				default:
					System.out.println("Wrong Option Selected!");
					
					
			}
			
			
			
			
			
	        
	        
	        
		    
		    
		}
	}
}
